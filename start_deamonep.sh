#!/bin/bash

Logep=$DIRBIN"/logep.sh"
if [ -z "$DIRBIN" ]
then
	echo "Error: el ambiente no fue inicializado."
	exit 1
else
	pid=`pgrep 'deamonep.sh'`
	if [ $? -eq 0 ]
	then
		$Logep "start_deamonep" "ERR" "Error: deamonep ya se está ejecutando bajo pid=${pid}."
		exit 1
	else
		$DIRBIN/deamonep.sh&
		pid=`pgrep 'deamonep.sh'`
		$Logep "start_deamonep" "INFO" "Se inicia la ejecución de deamonep bajo el pid=${pid}."
		exit 0
	fi
fi

