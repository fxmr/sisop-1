#!/bin/bash
#Mover archivos
#Recibo tres parametros, el primero es origen, segundo destino y tercero el COMANDO que llama a este.
ORIGEN="$1"
DESTINO="$2"
COMANDO="$3"
cant_par="$#"
pathorigen="${1%/*}"
pathdestino="${2%/*}"
archorigen="${1##*/}"

function verificar_parametros() {
  if [ $cant_par -gt 3 ]; then
      ./logep.sh $COMANDO  "Mover: Cantidad Invalida de parametros. No puede ingresar mas de 3 parámetros" "ERROR"
      exit 1
  fi
  if [ $cant_par -lt 2 ]; then
      ./logep.sh  $COMANDO  "Mover: Cantidad Invalida de parametros. No puede ingresar mas de 3 parámetros" "ERROR"
      exit 1
  fi
}

function verificar_existencia_directorios() {
  if [ $cant_par -eq 3 ]; then
    if [ ! -e "$ORIGEN" ]; then # si no existe el archivo o carpeta  origen logea
      ./logep.sh  $COMANDO "Mover:La carpeta o archivo  $ORIGEN  que desea mover no existe" "ERROR"
      exit 2
    fi
    if [ ! -d "$DESTINO" ]; then # sino existe el directorio
  	 ./logep.sh $COMANDO  "Mover: El directorio destino $pathdestino no existe" "ERROR"
      exit 2
    fi
fi
}

function verificar_son_iguales() {
  if [ "$pathorigen"  == "$pathdestino" ]; then
	if [ $cant_par -eq 3 ]; then
	   ./logep.sh $COMANDO  "Mover: El directorio de origen y de destino son iguales" "ERROR"
	fi
     exit 3
  fi
}

function mover_archivo () {

# Verifico si es un archivo dpllicado
  if [ -f "$pathdestino/$archorigen" ]; then

	  if [ ! -d "$pathdestino/dpl" ];then  # Si no existe el directorio de duplicados lo crea
	     mkdir "$pathdestino/dpl"
	  fi
	  # Verifico que no exista en el directorio de duplicados el mismo archivo. Si existe obtengo el numero de secuencia
	  nnn=$(ls "$pathdestino/dpl" | grep "^$archorigen.[0-9]\{1,3\}$" | sort -r | sed s/$archorigen.// | head -n 1)

	  if [ "$nnn" == "" ]; then  # Si no existen duplicados entonces le asigno a nnn 0
	      nnn=0
	  fi
 	  # Incremento el numero de secuencia para los archivos duplicados
	  nnn=$(echo $nnn + 1 | bc -l )
 	  mv "$ORIGEN" "$pathdestino/dpl/$archorigen.$nnn"

	  if [ $cant_par -eq 3 ]; then
	    ./logep.sh $COMANDO  "Mover: El archivo que intenta mover ya existe. Se copió al directorio de duplicados" "WARN"
	  fi
  else # Copio el archivo al directorio destino
       mv "$ORIGEN" "$pathdestino"
	  if [ $cant_par -eq 3 ];then
	     ./logep.sh $COMANDO  "Mover: Operacion Exitosa. El archivo se ha movido sin errores. Se movio de: $pathorigen hacia: $pathdestino" "INFO"
	  fi
       exit 0
  fi
}



#Comienza Mover----------------
verificar_parametros
verificar_existencia_directorios
verificar_son_iguales
mover_archivo
